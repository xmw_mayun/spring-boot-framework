package com.zeyu.framework.modules.test.dao;

import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.AutoDao;
import com.zeyu.framework.modules.test.entity.AutoTest;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 自动实现增删改查的接口,需要一个对应的xml文件
 * Created by zeyuphoenix on 2017/1/23.
 */
@MyBatisDao
public interface AutoTestDao extends AutoDao<AutoTest> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Select("SELECT * FROM tb_auto_test WHERE description = #{desc}")
    AutoTest findByDesc(@Param("desc") String  desc);

}
