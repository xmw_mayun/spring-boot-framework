package com.zeyu.framework.modules.sys.service;

import com.zeyu.framework.core.service.CrudService;
import com.zeyu.framework.modules.sys.dao.DictDao;
import com.zeyu.framework.modules.sys.entity.Dict;
import com.zeyu.framework.modules.sys.utils.DictUtils;
import com.zeyu.framework.utils.CacheUtils;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 字典Service
 */
@Service
@Transactional(readOnly = true)
public class DictService extends CrudService<DictDao, Dict> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Transactional
    @Override
    public void save(Dict dict) {
        super.save(dict);
        CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
    }

    @Transactional
    @Override
    public void delete(Dict dict) {
        super.delete(dict);
        CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
    }

    /**
     * 删除多个字典
     */
    @Transactional
    @Override
    public void deleteList(List<String> idList, Dict dict) {
        super.deleteList(idList, dict);
        CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 查询字段类型列表
     *
     * @return 类型列表
     */
    public List<String> findTypeList() {
        return dao.findTypeList(new Dict());
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
