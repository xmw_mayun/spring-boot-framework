package com.zeyu.framework.modules.sys.entity;

import com.zeyu.framework.core.persistence.entity.TreeEntity;

import org.hibernate.validator.constraints.Length;

/**
 * 部门entity
 * Created by zeyuphoenix on 16/7/30.
 */
public class Office extends TreeEntity<Office> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 类型（1：公司；2：部门；3：小组）
     */
    public static final String OFFICE_TYPE_COMPANY = "1";
    public static final String OFFICE_TYPE_DEPARTMENT = "2";
    public static final String OFFICE_TYPE_TEAM = "3";

    // ================================================================
    // Fields
    // ================================================================

    // 机构编码
    private String code;
    // 机构类型（1：公司；2：部门；3：小组）
    private String type;
    // 机构等级（1：一级；2：二级；3：三级；4：四级）
    private String grade;
    // 联系地址
    private String address;
    // 邮政编码
    private String zipCode;
    // 负责人
    private String master;
    // 电话
    private String phone;
    // 传真
    private String fax;
    // 邮箱
    private String email;
    // 是否可用
    private String useable;
    // 主负责人
    private User primaryPerson;
    // 副负责人
    private User deputyPerson;

    // ================================================================
    // Constructors
    // ================================================================

    public Office() {
        super();
        this.type = OFFICE_TYPE_DEPARTMENT;
    }

    public Office(String id) {
        super(id);
        this.type = OFFICE_TYPE_DEPARTMENT;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public Office getParent() {
        return parent;
    }

    @Override
    public void setParent(Office parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return name;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getUseable() {
        return useable;
    }

    public void setUseable(String useable) {
        this.useable = useable;
    }

    public User getPrimaryPerson() {
        return primaryPerson;
    }

    public void setPrimaryPerson(User primaryPerson) {
        this.primaryPerson = primaryPerson;
    }

    public User getDeputyPerson() {
        return deputyPerson;
    }

    public void setDeputyPerson(User deputyPerson) {
        this.deputyPerson = deputyPerson;
    }

    @Length(min = 1, max = 1)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Length(min = 1, max = 1)
    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Length(max = 255)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Length(max = 100)
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Length(max = 100)
    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    @Length(max = 200)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Length(max = 200)
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Length(max = 200)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Length(max = 100)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
