package com.zeyu.framework.modules.sys.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.core.web.BaseController;
import com.zeyu.framework.core.web.Result;
import com.zeyu.framework.modules.sys.entity.Office;
import com.zeyu.framework.modules.sys.entity.Role;
import com.zeyu.framework.modules.sys.entity.User;
import com.zeyu.framework.modules.sys.service.SystemService;
import com.zeyu.framework.modules.sys.utils.UserUtils;
import com.zeyu.framework.tools.PasswordGenerate;
import com.zeyu.framework.utils.BeanValidators;
import com.zeyu.framework.utils.DateUtils;
import com.zeyu.framework.utils.StringUtils;
import com.zeyu.framework.utils.excel.ExportExcel;
import com.zeyu.framework.utils.excel.ImportExcel;
import com.zeyu.framework.utils.excel.annotation.ExcelField;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户Controller
 */
@Controller
@RequestMapping(value = "/sys/user")
public class UserController extends BaseController {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private SystemService systemService;

    /**
     * 验证Bean实例对象
     */
    @Autowired
    protected Validator validator;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @ModelAttribute
    public User get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return systemService.getUser(id);
        } else {
            return new User();
        }
    }

    /**
     * 用户列表页
     */
    @RequiresPermissions("sys:user:view")
    @RequestMapping(value = {"", "index"})
    public String index(Model model) {
        model.addAttribute("allRoles", systemService.findAllRole());
        return "modules/sys/userList";
    }

    /**
     * 用户列表
     */
    @RequiresPermissions("sys:user:view")
    @RequestMapping(value = "list")
    @ResponseBody
    public Page<Map<String, Object>> list(HttpServletRequest request) {

        Page<User> page = new Page<>(request);
        // 查询扩展
        String extraSearch = page.getDatatablesCriterias().getExtraSearch();
        // 转换
        User user = JsonMapper.getInstance().fromJson(extraSearch, User.class);
        if (user == null) {
            user = new User();
        } else {
            // office.id 被json忽略,需要转换
            String officeId = readIgnoreValue(extraSearch, "office.id");
            if (StringUtils.isNotBlank(officeId)) {
                user.setOffice(new Office(officeId));
            }
        }
        Page<User> result = systemService.findUser(page, user);

        List<Map<String, Object>> tranMaps = Lists.newArrayList();
        if (result != null) {
            for (User item : result.getList()) {
                Map<String, Object> tran = Maps.newHashMap();
                tran.put("office.id", item.getOffice().getId());
                tran.put("office.name", item.getOffice().getName());
                tranMaps.add(tran);
            }
        }
        return transPage(result, tranMaps);

    }

    /**
     * 保存用户数据
     */
    @RequiresPermissions("sys:user:edit")
    @RequestMapping(value = "save")
    @ResponseBody
    public Result save(@Validated User user, HttpServletRequest request) {
        Result result = new Result();
        if (MODE_DEMO) {
            result.setStatus(Result.ERROR);
            result.setMessage("演示模式，不允许操作!");
            return result;
        }

        // 修正引用赋值问题，不知道为何，Office引用的一个实例地址，修改了一个，另外一个跟着修改。
        user.setOffice(new Office(request.getParameter("office.id")));
        // 如果新密码为空，则不更换密码
        if (StringUtils.isNotBlank(user.getNewPassword())) {
            user.setPassword(SystemService.entryptPassword(PasswordGenerate.md5(user.getNewPassword())));
        }

        if (!"true".equals(checkLoginName(user.getOldLoginName(), user.getLoginName()))) {
            result.setStatus(Result.ERROR);
            result.setMessage("保存用户'" + user.getLoginName() + "'失败，登录名已存在");
            return result;
        }
        // 角色数据有效性验证，过滤不在授权内的角色
        List<Role> roleList = Lists.newArrayList();
        List<String> roleIdList = user.getRoleIdList();
        roleList.addAll(systemService.findAllRole().stream()
                .filter(r -> roleIdList.contains(r.getId()))
                .collect(Collectors.toList()));
        user.setRoleList(roleList);
        // 保存用户信息
        systemService.saveUser(user);
        // 清除当前用户缓存
        if (user.getLoginName().equals(UserUtils.getUser().getLoginName())) {
            UserUtils.clearCache();
        }
        result.setStatus(Result.SUCCESS);
        result.setMessage("保存用户'" + user.getLoginName() + "'成功");

        return result;
    }

    /**
     * 删除用户
     */
    @RequiresPermissions("sys:user:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public Result delete(User user) {
        Result result = new Result();
        if (MODE_DEMO) {
            result.setStatus(Result.ERROR);
            result.setMessage("演示模式，不允许操作!");
            return result;
        }
        if (UserUtils.getUser().getId().equals(user.getId())) {
            result.setStatus(Result.ERROR);
            result.setMessage("删除用户失败, 不允许删除当前用户!");
            return result;
        } else if (User.isAdmin(user.getId())) {
            result.setStatus(Result.ERROR);
            result.setMessage("删除用户失败, 不允许删除超级管理员用户!");
            return result;
        } else {
            systemService.deleteUser(user);
            result.setStatus(Result.SUCCESS);
            result.setMessage("删除用户成功");
            return result;
        }
    }

    /**
     * 角色分配 -- 获取当前用户的角色列表
     *
     * @param id 用户
     * @return 角色列表
     */
    @RequiresPermissions("user")
    @ResponseBody
    @RequestMapping(value = "userRoles")
    public List<Role> roleUsers(String id) {
        User user = systemService.getUser(id);
        return user.getRoleList();
    }

    /**
     * 保存用户的角色列表
     *
     * @param id 用户id
     */
    @RequiresPermissions("user")
    @ResponseBody
    @RequestMapping(value = "assign")
    public Result assign(String id, String[] roleIds) {

        Result result = new Result();
        User user = systemService.getUser(id);
        if (!UserUtils.getUser().isAdmin()) {
            result.setStatus(Result.ERROR);
            result.setMessage("越权操作，只有超级管理员才能修改此数据!");
            return result;
        }
        if (MODE_DEMO) {
            result.setStatus(Result.ERROR);
            result.setMessage("演示模式，不允许操作!");
            return result;
        }
        // 需要验证删除、添加和不变的三种
        List<Role> roleList = user.getRoleList();
        List<Role> addRoleList = Lists.newArrayList();
        List<Role> delRoleList = Lists.newArrayList();
        // 返回信息
        StringBuilder builder = new StringBuilder("分配用户的角色操作完成,结果如下:<br/>");
        if (roleIds == null || roleIds.length == 0) {
            // 用户无角色
            result.setStatus(Result.ERROR);
            result.setMessage("设置失败,用户必须分配一个角色!");
            return result;

        } else {
            // 找到需要添加的
            for (String roleId : roleIds) {
                boolean exist = false;
                for (Role role : roleList) {
                    if (StringUtils.equals(roleId, role.getId())) {
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    addRoleList.add(systemService.getRole(roleId));
                }
            }
            // 找到需要删除的
            for (Role role : roleList) {
                boolean exist = false;
                for (String roleId : roleIds) {
                    if (StringUtils.equals(roleId, role.getId())) {
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    delRoleList.add(role);
                }
            }
        }

        int count = 0;
        for (Role role : delRoleList) {
            builder.append("角色【").append(role.getName()).append("】从用户【")
                    .append(user.getName()).append("】中移除成功!<br/>");
            count++;
        }
        // 无操作
        if (addRoleList.size() == 0 && delRoleList.size() == 0) {
            builder.append("用户角色分配无变化");
        }
        if (count > 0)
            builder.append("已经成功从用户【").append(user.getName()).append("】移除【")
                    .append(count).append("】个角色!");
        count = 0;
        for (Role role : addRoleList) {
            builder.append("新增角色【").append(role.getName()).append("】到用户【")
                    .append(user.getName()).append("】中成功!<br/>");
            count++;
        }
        if (count > 0)
            builder.append("已经成功从用户【").append(user.getName()).append("】增加【")
                    .append(count).append("】个角色!");

        List<Role> roles = Lists.newArrayList();
        for (String roleId : roleIds) {
            roles.add(new Role(roleId));
        }
        user.setRoleList(roles);
        systemService.assignRole(user);
        result.setStatus(Result.SUCCESS);
        result.setMessage(builder.toString());

        return result;
    }

    /**
     * 导出用户数据
     */
    @RequiresPermissions("sys:user:view")
    @RequestMapping(value = "export", method = RequestMethod.POST)
    @ResponseBody
    public Result exportFile(User user, HttpServletRequest request, HttpServletResponse response) {
        Result result = new Result();

        try {
            String fileName = "用户数据" + DateUtils.getDate("yyyyMMddHHmmss") + ".xlsx";
            Page<User> page = systemService.findUser(new Page<>(request, -1), user);
            logger.debug("export file name is {}", fileName);
            new ExportExcel("用户数据", User.class).setDataList(page.getList()).write(response, fileName).dispose();
            result.setStatus(Result.SUCCESS);
            result.setMessage("导出数据成功! ");
        } catch (Exception e) {
            result.setStatus(Result.ERROR);
            result.setMessage("导出用户失败！失败信息：" + e.getMessage());
        }


        return result;
    }

    /**
     * 导入用户数据
     */
    @RequiresPermissions("sys:user:edit")
    @RequestMapping(value = "import", method = RequestMethod.POST)
    @ResponseBody
    public Result importFile(MultipartFile file) {
        Result result = new Result();
        if (MODE_DEMO) {
            result.setStatus(Result.ERROR);
            result.setMessage("演示模式，不允许操作！");
            return result;
        }
        try {
            logger.debug("import file is {}", file);

            int successNum = 0;
            int failureNum = 0;
            StringBuilder failureMsg = new StringBuilder();

            ImportExcel ei = new ImportExcel(file, 1, 0);
            List<User> list = ei.getDataList(User.class);
            for (User user : list) {
                try {
                    if ("true".equals(checkLoginName("", user.getLoginName()))) {
                        user.setPassword(SystemService.entryptPassword("123456"));
                        BeanValidators.validateWithException(validator, user);
                        systemService.saveUser(user);
                        successNum++;
                    } else {
                        failureMsg.append("<br/>登录名 ").append(user.getLoginName()).append(" 已存在; ");
                        failureNum++;
                    }
                } catch (ConstraintViolationException ex) {
                    failureMsg.append("<br/>登录名 ").append(user.getLoginName()).append(" 导入失败：");
                    List<String> messageList = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
                    for (String message : messageList) {
                        failureMsg.append(message).append("; ");
                        failureNum++;
                    }
                } catch (Exception ex) {
                    failureMsg.append("<br/>登录名 ").append(user.getLoginName()).append(" 导入失败：").append(ex.getMessage());
                }
            }

            if (failureNum > 0) {
                failureMsg.insert(0, "，失败 " + failureNum + " 条用户，导入信息如下：");
            }
            result.setStatus(Result.SUCCESS);
            result.setMessage("已成功导入 " + successNum + " 条用户" + failureMsg);
        } catch (Exception e) {
            result.setStatus(Result.ERROR);
            result.setMessage("导入用户失败！失败信息：" + e.getMessage());
        }
        return result;
    }

    /**
     * 下载导入用户数据模板
     * @return 用户数据模板
     */
    @RequiresPermissions("sys:user:view")
    @RequestMapping(value = "import/template")
    @ResponseBody
    public Result importFileTemplate(HttpServletResponse response) {
        Result result = new Result();
        try {
            String fileName = "用户数据导入模板.xlsx";
            List<User> list = Lists.newArrayList();
            list.add(UserUtils.getUser());
            new ExportExcel("用户数据", User.class, ExcelField.Type.IMPORT).setDataList(list).write(response, fileName).dispose();
            result.setStatus(Result.SUCCESS);
            result.setMessage("导入用户数据模板下载成功！");
        } catch (Exception e) {
            result.setStatus(Result.ERROR);
            result.setMessage("导入模板下载失败！失败信息：" + e.getMessage());
        }
        return result;
    }

    /**
     * 验证登录名是否有效
     *
     * @param oldLoginName 旧登录名
     * @param loginName    登录名
     * @return 是否有效
     */
    @ResponseBody
    @RequiresPermissions("sys:user:edit")
    @RequestMapping(value = "checkLoginName")
    public String checkLoginName(String oldLoginName, String loginName) {
        if (loginName != null && loginName.equals(oldLoginName)) {
            return "true";
        } else if (loginName != null && systemService.getUserByLoginName(loginName) == null) {
            return "true";
        }
        return "false";
    }

    /**
     * 用户信息显示及保存
     *
     * @param user 用户
     * @return 是否有效
     */
    @RequiresPermissions("user")
    @RequestMapping(value = "info")
    @ResponseBody
    public Result info(User user) {
        User currentUser = UserUtils.getUser();
        Result result = new Result();
        if (StringUtils.isNotBlank(user.getName())) {
            if (MODE_DEMO) {
                result.setStatus(Result.ERROR);
                result.setMessage("演示模式，不允许操作!");
                return result;
            }
            currentUser.setEmail(user.getEmail());
            currentUser.setPhone(user.getPhone());
            currentUser.setMobile(user.getMobile());
            currentUser.setRemarks(user.getRemarks());
            currentUser.setPhoto(user.getPhoto());
            currentUser.setSex(user.getSex());
            currentUser.setAge(user.getAge());
            systemService.updateUserInfo(currentUser);
            result.setStatus(Result.SUCCESS);
            result.setMessage("保存用户信息成功!");
        } else {
            result.setStatus(Result.ERROR);
            result.setMessage("用户错误,不能保存!");
        }
        return result;
    }

    /**
     * 返回用户信息
     *
     * @return 用户信息
     */
    @RequiresPermissions("user")
    @ResponseBody
    @RequestMapping(value = "infoData")
    public Map<String, Object> infoData(String id) {

        User user = systemService.getUser(id);

        // 不能为空,设置默认值
        if (user.getOffice() == null || user.getOffice().getId() == null) {
            user.setOffice(UserUtils.getUser().getOffice());
        }

        Map<String, Object> tran = Maps.newHashMap();
        tran.put("office.id", user.getOffice().getId());
        tran.put("office.name", user.getOffice().getName());
        tran.put("createName", UserUtils.getUserName(user.getCreateBy().getId()));
        tran.put("roleIdList", user.getRoleIdList());

        return transEntity(user, tran);
    }

    /**
     * 修改个人用户密码
     *
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     */
    @RequiresPermissions("user")
    @RequestMapping(value = "modifyPwd")
    @ResponseBody
    public Result modifyPwd(String oldPassword, String newPassword) {
        User user = UserUtils.getUser();
        Result result = new Result();
        if (StringUtils.isNotBlank(oldPassword) && StringUtils.isNotBlank(newPassword)) {
            if (MODE_DEMO) {
                result.setStatus(Result.ERROR);
                result.setMessage("演示模式，不允许操作!");
                return result;
            }
            if (SystemService.validatePassword(oldPassword, user.getPassword())) {
                systemService.updatePasswordById(user.getId(), user.getLoginName(), newPassword);
                result.setStatus(Result.SUCCESS);
                result.setMessage("修改密码成功");
            } else {
                result.setStatus(Result.ERROR);
                result.setMessage("修改密码失败，旧密码错误");
            }
        } else {
            result.setStatus(Result.ERROR);
            result.setMessage("不能修改密码!");
        }

        return result;
    }

    /**
     * 获取office下所有用户
     */
    @RequiresPermissions("user")
    @ResponseBody
    @RequestMapping(value = "treeData")
    public List<Map<String, Object>> treeData(@RequestParam(required = false) String officeId) {
        List<Map<String, Object>> mapList = Lists.newArrayList();
        List<User> list = systemService.findUserByOfficeId(officeId);
        for (User e : list) {
            Map<String, Object> map = Maps.newHashMap();
            map.put("id", "u_" + e.getId());
            map.put("pId", officeId);
            // 为树增加图标
            String name = "<i class='fa fa-user alert-success'></i> " + StringUtils.replace(e.getName(), " ", "");
            map.put("name", name);
            mapList.add(map);
        }
        return mapList;
    }

    /**
     * 构建office-user树,其中user-id加入u-
     *
     * @return office-user tree
     */
    @RequiresPermissions("user")
    @ResponseBody
    @RequestMapping(value = "ouTreeData")
    public List<Map<String, Object>> ouTreeData(@RequestParam(required = false) String extId) {
        List<Map<String, Object>> mapList = Lists.newArrayList();
        List<Office> list = UserUtils.getOfficeList();
        list.stream().filter(e -> (StringUtils.isBlank(extId) || (!extId.equals(e.getId())
                && !e.getParentIds().contains("," + extId + ",")))
                && YES.equals(e.getUseable()))
                .forEach(e -> {
                    Map<String, Object> map = Maps.newHashMap();
                    map.put("id", e.getId());
                    map.put("pId", e.getParentId());
                    map.put("pIds", e.getParentIds());
                    String oname = "<i class='" + UserUtils.getIconByType(e) + "'></i> " + StringUtils.replace(e.getName(), " ", "");
                    map.put("name", oname);
                    map.put("isParent", true);

                    // 添加office下的用户
                    List<User> userList = systemService.findUserByOfficeId(e.getId());
                    for (User user : userList) {
                        Map<String, Object> umap = Maps.newHashMap();
                        umap.put("id", "u_" + user.getId());
                        umap.put("pId", user.getId());
                        // 为树增加图标
                        String name = "<i class='fa fa-user alert-success'></i> " + StringUtils.replace(user.getName(), " ", "");
                        umap.put("name", name);
                        mapList.add(umap);
                    }

                    mapList.add(map);
                });
        return mapList;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
