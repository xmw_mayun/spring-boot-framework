package com.zeyu.framework.modules.sys.dao;


import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.modules.sys.entity.Dict;

import java.util.List;

/**
 * 字典DAO接口
 */
@MyBatisDao
public interface DictDao extends CrudDao<Dict> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取类型列表
     *
     * @param dict 对象
     * @return 类型列表
     */
    List<String> findTypeList(Dict dict);

}
