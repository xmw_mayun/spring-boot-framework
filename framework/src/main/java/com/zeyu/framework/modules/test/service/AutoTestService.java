package com.zeyu.framework.modules.test.service;

import com.zeyu.framework.core.service.AutoService;
import com.zeyu.framework.modules.test.entity.AutoTest;
import com.zeyu.framework.modules.test.dao.AutoTestDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 自动生成增删改查的service,默认有一些基本方法
 * Created by zeyuphoenix on 2017/1/23.
 */
@Service
@Transactional(readOnly = true)
public class AutoTestService extends AutoService<AutoTestDao, AutoTest> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    public AutoTest findByDesc(String desc) {
        return dao.findByDesc(desc);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
