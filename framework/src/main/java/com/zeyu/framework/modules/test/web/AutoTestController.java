package com.zeyu.framework.modules.test.web;

import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.core.web.BaseController;
import com.zeyu.framework.modules.test.entity.AutoTest;
import com.zeyu.framework.modules.test.service.AutoTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 测试自动增删改查controller
 * Created by zeyuphoenix on 2017/1/23.
 */
@RestController
@RequestMapping("/auto/test")
public class AutoTestController extends BaseController {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private AutoTestService autoTestService;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @RequestMapping("insert")
    public ResponseEntity<String> insert(@RequestBody AutoTest autoTest) {

        logger.info("add auto define dao ");

        autoTestService.save(autoTest);
        return ResponseEntity.ok("add auto test success");
    }

    @RequestMapping(value = "delete/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {

        logger.info("delete auto define dao ");
        AutoTest autoTest = new AutoTest();
        autoTest.setId(id);
        autoTestService.delete(autoTest);
        return ResponseEntity.ok("delete auto test success");
    }

    @RequestMapping(value = "findAll")
    public ResponseEntity<List<AutoTest>> findAll() {

        logger.info("find all, use auto select.");

        return ResponseEntity.ok(autoTestService.findList(new AutoTest()));
    }

    @RequestMapping(value = "findByPage")
    public ResponseEntity<Page<AutoTest>> findByPage() {

        logger.info("find by page, use auto select.");
        Page<AutoTest> page = new Page<>();

        return ResponseEntity.ok(autoTestService.findPage(page, new AutoTest()));
    }

    @RequestMapping("findByDesc/{desc}")
    public ResponseEntity<AutoTest> findByDesc(@PathVariable("desc") String desc) {

        logger.info("find auto test by description, use user define method ");

        return ResponseEntity.ok(autoTestService.findByDesc(desc));
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
