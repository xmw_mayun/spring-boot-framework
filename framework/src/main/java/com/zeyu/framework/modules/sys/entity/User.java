package com.zeyu.framework.modules.sys.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.zeyu.framework.core.persistence.entity.BaseEntity;
import com.zeyu.framework.core.persistence.entity.DataEntity;
import com.zeyu.framework.core.security.interfaces.SecurityUser;
import com.zeyu.framework.utils.Collections3;
import com.zeyu.framework.utils.excel.annotation.ExcelField;
import com.zeyu.framework.utils.excel.fieldtype.RoleListType;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户Entity
 */
public class User extends DataEntity<User> implements SecurityUser {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 归属部门
    private Office office;
    // 登录名
    private String loginName;
    // 名称
    protected String name;
    // 密码
    private String password;
    // 工号
    private String no;
    // 性别
    private int sex = 0;
    // 年龄
    private String age = "18";
    // 邮箱
    private String email;
    // 电话
    private String phone;
    // 手机
    private String mobile;
    // 用户类型
    private String type;
    // 最后登陆IP
    private String loginIp;
    // 最后登陆日期
    private Date loginDate;
    // 是否允许登陆
    private String loginFlag;
    // 头像
    private String photo;
    // 原登录名
    private String oldLoginName;
    // 新密码
    private String newPassword;
    // 用户当前状态 (0:正常;1:锁定;2:已过期;)
    private int status = 0;

    // 上次登陆IP
    private String oldLoginIp;
    // 上次登陆日期
    private Date oldLoginDate;

    // 根据角色查询用户条件
    private Role role;

    // 拥有角色列表
    private List<Role> roleList = Lists.newArrayList();

    // ================================================================
    // Constructors
    // ================================================================

    public User() {
        super();
        this.loginFlag = YES;
    }

    public User(String id) {
        super(id);
    }

    public User(String id, String loginName) {
        super(id);
        this.loginName = loginName;
    }

    public User(Role role) {
        super();
        this.role = role;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public String toString() {

        return id;
    }

    @Override
    @JsonIgnore
    public List<String> getRoleNameList() {

        return roleList.stream().map(Role::getName).collect(Collectors.toList());
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 用户拥有的角色名称字符串, 多个角色名称用','分隔.
     */
    public String getRoleNames() {
        return Collections3.extractToString(roleList, "name", ",");
    }

    public boolean isAdmin() {
        return isAdmin(this.id);
    }

    public static boolean isAdmin(String id) {
        return id != null && DEFAULT_ROOT_ID.equals(id);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLoginFlag() {
        return loginFlag;
    }

    public void setLoginFlag(String loginFlag) {
        this.loginFlag = loginFlag;
    }

    @ExcelField(title = "ID", type = ExcelField.Type.EXPORT, align = ExcelField.Align.CENTER, sort = 1)
    public String getId() {
        return id;
    }

    @JsonIgnore
    @NotNull(message = "归属部门不能为空")
    @ExcelField(title = "归属部门", align = ExcelField.Align.CENTER, sort = 20)
    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    @Length(min = 1, max = 100, message = "登录名长度必须介于 1 和 100 之间")
    @ExcelField(title = "登录名", align = ExcelField.Align.CENTER, sort = 30)
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @JsonIgnore
    @Length(min = 1, max = 100, message = "密码长度必须介于 1 和 100 之间")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Length(min = 1, max = 100, message = "姓名长度必须介于 1 和 100 之间")
    @ExcelField(title = "姓名", align = ExcelField.Align.CENTER, sort = 40)
    public String getName() {
        return name;
    }

    @Length(min = 1, max = 100, message = "工号长度必须介于 1 和 100 之间")
    @ExcelField(title = "工号", align = ExcelField.Align.CENTER, sort = 45)
    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Email(message = "邮箱格式不正确")
    @Length(max = 200, message = "邮箱长度必须介于 1 和 200 之间")
    @ExcelField(title = "邮箱", align = ExcelField.Align.LEFT, sort = 50)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Length(max = 200, message = "电话长度必须介于 1 和 200 之间")
    @ExcelField(title = "电话", align = ExcelField.Align.CENTER, sort = 60)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Length(max = 200, message = "手机长度必须介于 1 和 200 之间")
    @ExcelField(title = "手机", align = ExcelField.Align.CENTER, sort = 70)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @ExcelField(title = "备注", align = ExcelField.Align.LEFT, sort = 900)
    public String getRemarks() {
        return remarks;
    }

    @Length(max = 100, message = "用户类型长度必须介于 1 和 100 之间")
    @ExcelField(title = "用户类型", align = ExcelField.Align.CENTER, sort = 80, dictType = "sys_user_type")
    public String getType() {
        return type;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    @ExcelField(title = "创建时间", type = ExcelField.Type.IMPORT_EXPORT, align = ExcelField.Align.LEFT, sort = 90)
    public Date getCreateDate() {
        return createDate;
    }

    @ExcelField(title = "最后登录IP", type = ExcelField.Type.EXPORT, align = ExcelField.Align.LEFT, sort = 100)
    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ExcelField(title = "最后登录日期", type = ExcelField.Type.EXPORT, align = ExcelField.Align.LEFT, sort = 110)
    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getOldLoginName() {
        return oldLoginName;
    }

    public void setOldLoginName(String oldLoginName) {
        this.oldLoginName = oldLoginName;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldLoginIp() {
        if (oldLoginIp == null) {
            return loginIp;
        }
        return oldLoginIp;
    }

    public void setOldLoginIp(String oldLoginIp) {
        this.oldLoginIp = oldLoginIp;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getOldLoginDate() {
        if (oldLoginDate == null) {
            return loginDate;
        }
        return oldLoginDate;
    }

    public void setOldLoginDate(Date oldLoginDate) {
        this.oldLoginDate = oldLoginDate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @JsonIgnore
    @ExcelField(title = "拥有角色", align = ExcelField.Align.LEFT, sort = 800, fieldType = RoleListType.class)
    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    @JsonIgnore
    public List<String> getRoleIdList() {
        List<String> roleIdList = Lists.newArrayList();


        roleIdList.addAll(roleList.stream().map(BaseEntity::getId).collect(Collectors.toList()));
        return roleIdList;
    }

    public void setRoleIdList(List<String> roleIdList) {
        roleList = Lists.newArrayList();
        for (String roleId : roleIdList) {
            Role role = new Role();
            role.setId(roleId);
            roleList.add(role);
        }
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
