package com.zeyu.framework.modules.sys.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zeyu.framework.core.persistence.entity.SimpleEntity;

import java.util.Date;

/**
 * 第三方登录提供者数据保存表
 * Created by zeyuphoenix on 2017/1/3.
 */
public class UserLoginProvider extends SimpleEntity<UserLoginProvider> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // 所属用户
    private User user;
    // 第三方编号
    private String validateId;
    // 类型(0:微信;1:QQ;2:新浪微博;3:Google;4:Yahoo;5:Hotmail;6:GitHub;7:OsChina)
    private String type = "weixin";
    // 当前状态 (0:正常;1:异常;)
    private int status = 0;
    // 第三方主页url地址
    private String url;
    // 创建日期
    private Date createDate;
    // 备注
    private String remarks;

    // ================================================================
    // Constructors
    // ================================================================

    /**
     * constructor
     */
    public UserLoginProvider() {
        super();
        this.type = "weixin";
        this.status = 0;
    }

    public UserLoginProvider(String id) {
        super(id);
    }

    public UserLoginProvider(String id, User user) {
        this(id);
        this.user = user;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getValidateId() {
        return validateId;
    }

    public void setValidateId(String validateId) {
        this.validateId = validateId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
