package com.zeyu.framework.tools.mq.receiver;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

/**
 * 旧式的实现接口方式的Listener,需要把这个接口在Configuration中注册
 * Created by zeyuphoenix on 2016/12/31.
 */
@Component
public class LowestMessageListener implements ChannelAwareMessageListener /* or MessageListener */ {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(LowestMessageListener.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        logger.info("==============================");
        logger.info(message.toString());
        logger.info("==============================");
        //确认消息成功消费
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
