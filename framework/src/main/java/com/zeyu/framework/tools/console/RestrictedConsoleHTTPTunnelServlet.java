package com.zeyu.framework.tools.console;

import org.apache.guacamole.GuacamoleException;
import org.apache.guacamole.net.GuacamoleTunnel;
import org.apache.guacamole.servlet.GuacamoleHTTPTunnelServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * Connects to a tunnel associated with the connection
 * Created by zeyuphoenix on 16/10/17.
 */
@WebServlet(urlPatterns = "/console/tunnel", description = "console tunnel use http.")
public class RestrictedConsoleHTTPTunnelServlet extends GuacamoleHTTPTunnelServlet {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(RestrictedConsoleHTTPTunnelServlet.class);

    // ================================================================o
    // Fields
    // ================================================================

    /**
     * Service for handling tunnel requests.
     */
    @Autowired
    private TunnelRequestService tunnelRequestService;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    protected GuacamoleTunnel doConnect(HttpServletRequest request) throws GuacamoleException {
        // Attempt to create HTTP tunnel
        GuacamoleTunnel tunnel = tunnelRequestService.createTunnel(new HTTPTunnelRequest(request));

        // If successful, warn of lack of WebSocket
        logger.info("Using HTTP tunnel (not WebSocket). Performance may be sub-optimal.");

        return tunnel;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
