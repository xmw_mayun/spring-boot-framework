package com.zeyu.framework.tools.report.convert.pool;

/**
 * 对象池接口,提供基本操作
 * Created by zeyuphoenix on 16/9/3.
 */
public interface ObjectPool<T> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 创建对象
     */
    void createObject();

    /**
     * 销毁对象
     */
    void destroyObject(T object);

    /**
     * 借出对象
     */
    T borrowObject() throws InterruptedException, PoolException;

    /**
     * 返回对象
     */
    void returnObject(T object, boolean validate) throws InterruptedException;

    /**
     * 对象池清除
     */
    void poolCleaner() throws PoolException, InterruptedException;

}
