package com.zeyu.framework.tools.report.convert.pool;

/**
 * 对象工厂接口
 * Created by zeyuphoenix on 16/9/3.
 */
public interface ObjectFactory<T> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 创建对象
     */
    T create();

    /**
     * 校验对象
     */
    boolean validate(T object);

    /**
     * 销毁对象
     */
    void destroy(T object);

    /**
     * 激活对象
     */
    void activate(T object);

    /**
     * 冻结对象
     */
    void passivate(T Object);

}
