package com.zeyu.framework.tools.docconvert.struct;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;

/**
 * 需要转换的文件信息
 * Created by zeyuphoenix on 2017/2/27.
 */
public class DocFileInfo {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /**
     * 文件名称
     */
    private String name;

    /**
     * 文件hash
     */
    private String hash;

    /**
     * 文件保存路径
     */
    private String path;

    /**
     * 文件pdf路径
     */
    private String pdfPath;

    /**
     * 文件swf路径
     */
    private String swfPath;

    /**
     * 封面缩略图路径
     */
    private String thumPath;

    /**
     * 文件扩展名
     */
    private String ext;

    /**
     * 文件大小(字节)
     */
    private Long size;

    /**
     * 文档总页数
     */
    private Integer pages;

    /**
     * 文档转换状态 0-待转换 1-转换中 2-转换完成 3-转换失败，默认0
     */
    private int conState;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPdfPath() {
        return pdfPath;
    }

    public void setPdfPath(String pdfPath) {
        this.pdfPath = pdfPath;
    }

    public String getSwfPath() {
        return swfPath;
    }

    public void setSwfPath(String swfPath) {
        this.swfPath = swfPath;
    }

    public String getThumPath() {
        return thumPath;
    }

    public void setThumPath(String thumPath) {
        this.thumPath = thumPath;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public int getConState() {
        return conState;
    }

    public void setConState(int conState) {
        this.conState = conState;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
