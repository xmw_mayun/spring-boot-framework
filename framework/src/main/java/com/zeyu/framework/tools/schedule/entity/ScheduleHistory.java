package com.zeyu.framework.tools.schedule.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zeyu.framework.core.persistence.entity.SimpleEntity;

import java.util.Date;

/**
 * 任务历史Entity
 * Created by zeyuphoenix on 16/9/20.
 */
public class ScheduleHistory extends SimpleEntity<ScheduleHistory> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 所属组
    private String group;
    // 名称
    private String name;
    // 任务类型: 0: 周期,1:cron表达式
    private int type = 1;
    // 描述
    private String description;
    // 任务开始时间
    private Date startTime;
    // '任务结束时间'
    private Date finishTime;
    // 任务执行时间信息
    private String cron;
    // 任务结果，0:正常,1:异常
    private int status = 0;
    // 任务持续时间
    private int duration = 0;

    // ================================================================
    // Constructors
    // ================================================================

    public ScheduleHistory() {
        super();
    }

    public ScheduleHistory(String id) {
        super(id);
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
