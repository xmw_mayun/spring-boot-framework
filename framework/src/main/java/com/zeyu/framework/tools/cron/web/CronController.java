package com.zeyu.framework.tools.cron.web;

import com.zeyu.framework.core.web.BaseController;
import com.zeyu.framework.tools.cron.entity.CronCriterias;
import com.zeyu.framework.tools.cron.entity.CronOption;
import com.zeyu.framework.tools.cron.entity.CronResult;
import com.zeyu.framework.tools.cron.service.CronService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 任务Cron表达式Controller
 *
 * <pre>
 *
 * 配置语法
 * 序号	项	是否必填	允许填写的值	     允许的通配符
 * 1	秒	  是	    0-59	           , - * /
 * 2	分	  是	    0-59	           , - * /
 * 3	小时  是	    0-23	           , - * /
 * 4	日	  是	    1-31	           , - * ? / L W
 * 5	月	  是	    1-12 or JAN-DEC    , - * /
 * 6	周	  是	    1-7 or SUN-SAT	   , - * ? / L #
 * 7	年	  否	    empty 或 1970-2099  , - * /
 *
 *
 * //0 0 12 * * ?	在每天中午12：00触发
 * //0 15 10 ? * *	每天上午10:15 触发
 * //0 15 10 * * ?	每天上午10:15 触发
 * //0 15 10 * * ? *	每天上午10:15 触发
 * //0 15 10 * * ? 2005	在2005年中的每天上午10:15 触发
 * //0 * 14 * * ?	每天在下午2：00至2：59之间每分钟触发一次
 * //0 0/5 14 * * ?	每天在下午2：00至2：59之间每5分钟触发一次
 * //0 0/5 14,18 * * ?	每天在下午2：00至2：59和6：00至6：59之间的每5分钟触发一次
 * //0 0-5 14 * * ?	每天在下午2：00至2：05之间每分钟触发一次
 * //0 10,44 14 ? 3 WED	每三月份的星期三在下午2：00和2：44时触发
 * //0 15 10 ? * MON-FRI	从星期一至星期五的每天上午10：15触发
 * //0 15 10 15 * ?	在每个月的每15天的上午10：15触发
 * //0 15 10 L * ?	在每个月的最后一天的上午10：15触发
 * //0 15 10 ? * 6L	在每个月的最后一个星期五的上午10：15触发
 * //0 15 10 ? * 6L 2002-2005	在2002, 2003, 2004 and 2005年的每个月的最后一个星期五的上午10：15触发
 * //0 15 10 ? * 6#3	在每个月的第三个星期五的上午10：15触发
 * //0 0 12 1/5 * ?	从每月的第一天起每过5天的中午12：00时触发
 * // 0 11 11 11 11 ? 在每个11月11日的上午11：11时触发
 * </pre>
 */
@Controller
@RequestMapping(value = "/tools/cron/")
public class CronController extends BaseController {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /**
     * Cron构建管理
     */
    private CronService cronService = CronService.getInstance();

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取Cron表达式
     */
    @RequestMapping(value = "create")
    @ResponseBody
    public CronResult create(CronCriterias cronCriterias) {
        return cronService.createCronResult(cronCriterias);
    }

    /**
     * 反解析Cron表达式生成页面项
     */
    @RequestMapping(value = "parse")
    @ResponseBody
    public CronResult parse(String cronExpression) {
        logger.info("反解析Cron表达式生成页面项.");
        CronCriterias cronCriterias = new CronCriterias();
        cronCriterias.setCronExpression(cronExpression);
        return cronService.createCronResult(cronCriterias);
    }

    /**
     * 解析页面参数生成Cron表达式
     */
    @RequestMapping(value = "build")
    @ResponseBody
    public CronResult build(List<CronOption> cronOptions) {
        logger.info("解析页面参数生成Cron表达式.");
        CronCriterias cronCriterias = new CronCriterias();
        cronCriterias.setCronOptions(cronOptions);
        return cronService.createCronResult(cronCriterias);
    }

    @RequestMapping(value = "help")
    public String help() {
        return "modules/tools/cronHelp";
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
