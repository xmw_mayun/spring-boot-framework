package com.zeyu.framework.tools.schedule.job;

import com.zeyu.framework.tools.schedule.service.JobService;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * quartz simple job, can like it.
 * Created by zeyuphoenix on 16/6/20.
 */
public class SimpleJob extends AbstractJob {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private JobService jobService;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public void process() throws Exception {
        logger.debug("execute simple job");
        logger.debug("job data info: {}", getJobDataMap());
        // do self work in here.
        logger.debug("job entity info is: {}", "");
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
