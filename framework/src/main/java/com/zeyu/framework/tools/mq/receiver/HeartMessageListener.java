package com.zeyu.framework.tools.mq.receiver;

import com.zeyu.framework.core.configuration.RabbitMqConfiguration;
import com.zeyu.framework.tools.mq.struct.MQMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * 心跳信息监听处理器
 * Created by zeyuphoenix on 2016/12/31.
 */
@Component
// @RabbitListener可以修饰Class，表示被修饰的类为消息监听器类，这时候可以使用@RabbitlHandler修饰消息处理方法。
// 同一个消息处理器类中，可以修饰多个消息处理方法，消息处理器会根据监听到的不同消息类型，调用相应的消息处理方法
// 验证后发现,同一个queue, 类上的@RabbitListener > 方法上@RabbitListener > MessageListener接口
@RabbitListener(queues = RabbitMqConfiguration.QUEUE_HEART) //启用Rabbit队列监听heart
public class HeartMessageListener {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(HeartMessageListener.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // 接收到消息处理
    @RabbitHandler
    public void process(@Payload MQMessage message) {
        logger.info("接收到心跳信息: " + message);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
