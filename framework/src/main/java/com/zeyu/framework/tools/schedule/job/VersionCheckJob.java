package com.zeyu.framework.tools.schedule.job;

import com.zeyu.framework.tools.schedule.annotation.AutoConfigurationJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.concurrent.TimeUnit;

/**
 * 这个job使用了主动配置注解,将会自动配置到quartz
 * Created by zeyuphoenix on 16/6/21.
 */
@AutoConfigurationJob(
        name = "注解任务-验证版权信息",
        group = "版权验证组",
        startDate = "1984-12-10 02:30:00",
        interval = 10,
        unit = TimeUnit.MINUTES
)
public class VersionCheckJob extends AbstractJob {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private JavaMailSender mailSender;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public void process() throws Exception {

        if (!this.getClass().getName().contains("com.zeyu.framework")) {
            logger.debug("execute version check job is error. ");
            logger.info("send version check mail starting");

            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("18515445588@163.com");
            message.setTo("zeyuphoenix@163.com");
            message.setSubject("主题: 使用了错误版权信息的代码");
            message.setText("使用了修改版权的代码,目前的路径是: " + this.getClass().getName());
            mailSender.send(message);

            logger.info("send version check mail ending");
        }
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
