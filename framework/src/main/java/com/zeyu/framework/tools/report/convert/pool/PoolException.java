package com.zeyu.framework.tools.report.convert.pool;

/**
 * 转换图片线程池异常类
 * Created by zeyuphoenix on 16/9/3.
 */
public class PoolException extends Exception {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 异常来源
    private String mistake;

    // ================================================================
    // Constructors
    // ================================================================

    /**
     * 构造
     */
    public PoolException() {
        super();
        mistake = "unknown to men";
    }

    /**
     * 构造
     */
    public PoolException(String err) {
        // call super class constructor
        super(err);
        // save message
        mistake = err;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     */
    public String getError() {
        return mistake;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
