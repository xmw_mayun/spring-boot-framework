package com.zeyu.framework.tools.schedule.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.core.service.CrudService;
import com.zeyu.framework.tools.schedule.dao.ScheduleHistoryDao;
import com.zeyu.framework.tools.schedule.entity.ScheduleHistory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 机构Service
 */
@Service
@Transactional(readOnly = true)
public class ScheduleHistoryService extends CrudService<ScheduleHistoryDao, ScheduleHistory> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /**
     * 查询分页数据
     *
     * @param page   分页对象
     * @param entity 实体
     * @return 查询结果
     */
    @Override
    public Page<ScheduleHistory> findPage(Page<ScheduleHistory> page, ScheduleHistory entity) {

        //获取第n页，m条内容，默认查询总数count
        PageHelper.startPage(page.getPageNum(), page.getMaxResults());

        // 显示为List,实际是Page对象
        entity.setPage(page);
        List<ScheduleHistory> list = dao.findByGroupAndName(entity);

        //用PageInfo对结果进行包装
        PageInfo<ScheduleHistory> pageInfo = new PageInfo<>(list);
        page.setList(pageInfo.getList());
        page.setCount(pageInfo.getTotal());

        return page;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
