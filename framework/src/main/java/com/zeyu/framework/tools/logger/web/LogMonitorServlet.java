package com.zeyu.framework.tools.logger.web;

import com.zeyu.framework.tools.logger.WebPageAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 异步获取log日志信息
 * Note: AsyncContext 存在无法关闭的问题,暂时使用web socket替换
 * Created by zeyuphoenix on 16/10/9.
 */
@WebServlet(urlPatterns = {"/logMonitor"}, asyncSupported = true)
public class LogMonitorServlet extends HttpServlet {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(LogMonitorServlet.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /**
     * 注册 监听Logger的消息队列
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 设置返回参数
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "private");
        response.setHeader("Pragma", "no-cache");
        response.setCharacterEncoding("UTF-8");

        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        final AsyncContext ac = request.startAsync();
        ac.setTimeout(60 * 60 * 1000L);//1小时

        ac.addListener(new AsyncListener() {
            public void onComplete(AsyncEvent event) throws IOException {
                ac.getResponse().getWriter().close();
                WebPageAppender.deleteAsyncContext(ac);
                logger.info("AsyncListener onComplete");
            }

            public void onTimeout(AsyncEvent event) throws IOException {
                ac.getResponse().getWriter().close();
                WebPageAppender.deleteAsyncContext(ac);
                logger.info("AsyncListener onTimeout");
            }

            public void onError(AsyncEvent event) throws IOException {
                ac.getResponse().getWriter().close();
                WebPageAppender.deleteAsyncContext(ac);
                logger.info("AsyncListener onError");
            }

            public void onStartAsync(AsyncEvent event) throws IOException {
                logger.info("AsyncListener onStartAsync");
            }
        });
        WebPageAppender.addAsyncContext(ac);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
