package com.zeyu.framework.utils.mybatis.generate;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.zeyu.framework.utils.mybatis.annotation.DaoAutoGenerate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 自动生成dao的环境参数
 * Created by zeyuphoenix on 2016/11/2.
 */
public class DaoEnvironment {

    // ================================================================
    // Constants
    // ================================================================
    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(DaoEnvironment.class);

    // ================================================================
    // Fields
    // ================================================================

    /**
     * 表名
     */
    private String tableName;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 类型参数
     */
    Type typeParameter;

    // ================================================================
    // Constructors
    // ================================================================

    /**
     * 构造函数
     * @param daoAutoGenerate 自动生成注解信息
     * @param classSymbol 类定义符号信息
     */
    public DaoEnvironment(DaoAutoGenerate daoAutoGenerate, Symbol.ClassSymbol classSymbol) {
        // 获取时间信息
        this.createTime = daoAutoGenerate.createTime();
        this.updateTime = daoAutoGenerate.updateTime();
        // dao的类名
        String daoClassName = classSymbol.getSimpleName().toString();
        if (!daoAutoGenerate.tableName().isEmpty())
            this.tableName = daoAutoGenerate.tablePrefix() + daoAutoGenerate.tableName();
            //"Activity{Dao}"
        else
            this.tableName = daoAutoGenerate.tablePrefix() + daoClassName.subSequence(0, daoClassName.length() - 3);
        if (classSymbol.getInterfaces() != null) {
            classSymbol.getInterfaces()
                    .forEach(i -> {
                        if (i.getTypeArguments() != null && !i.getTypeArguments().isEmpty()) {
                            if (i.getTypeArguments().size() > 1 || this.typeParameter != null)
                                throw new RuntimeException("不支持多个类型参数");
                            this.typeParameter = i.getTypeArguments().get(0);
                        }
                    });
        }
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * 获取表名
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * 获取更新时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * 获取创建时间
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     *
     */
    public Symbol.TypeSymbol getRealTypeByTypeParameter(Type type) {

        if (typeParameter == null) {
            logger.warn("类型错误,获取的类型是: {}", type);
            throw new RuntimeException("未从类定义中获取类型变量实例,请查看配置是否正确");
        }
        return typeParameter.tsym;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
