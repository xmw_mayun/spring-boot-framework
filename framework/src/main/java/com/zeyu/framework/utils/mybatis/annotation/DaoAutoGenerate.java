package com.zeyu.framework.utils.mybatis.annotation;

/**
 * 自动生成dao的接口方法和xml-sql实现
 * Created by zeyuphoenix on 2016/11/2.
 */
public @interface DaoAutoGenerate {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 插入方法的前缀
     */
    String[] insertPrefix() default {"insert"};

    /**
     * 插入列表方法的前缀
     */
    String[] batchInsertPrefix() default {"batchInsert"};

    /**
     * 查询方法的前缀
     */
    String[] findPrefix() default {"findBy"};

    /**
     * 删除方法的前缀
     */
    String[] deletePrefix() default {"deleteBy"};

    /**
     * 查询方法的前缀
     */
    String[] queryPrefix() default {"queryBy"};

    /**
     * 查询方法的前缀
     */
    String[] queryInPrefix() default {"queryIn"};

    /**
     * 更新方法的前缀
     */
    String[] updatePrefix() default {"update"};

    /**
     * 更新方法的前缀
     */
    String[] updateForPrefix() default {"updateFor"};

    /**
     * 统计方法的前缀
     */
    String[] countPrefix() default {"countBy"};

    /**
     * 统计方法的前缀
     */
    String[] countInPrefix() default {"countIn"};

    /**
     * 统计所有方法的前缀
     */
    String[] countAllPrefix() default {"countAll"};

    /**
     * 查询所有方法的前缀
     */
    String[] queryAllPrefix() default {"queryAll"};

    String separator() default "And";

    /**
     * 数据库表名的前缀
     */
    String tablePrefix() default "tb_";

    /**
     * 数据表的默认主键
     */
    String primaryKey() default "id";

    /**
     * 创建时间
     */
    String createTime() default "createTime";

    /**
     * 更新时间
     */
    String updateTime() default "updateTime";

    /**
     * 数据表的名称
     */
    String tableName() default "";

    /**
     * 默认排序语句
     */
    String orderBy() default "OrderBy";

    /**
     * 排序
     */
    String orderByWith() default "With";
}
