package com.zeyu.framework.core.persistence.dao;

import com.zeyu.framework.core.persistence.entity.TreeEntity;

import java.util.List;

/**
 * DAO支持类实现
 *
 * @param <T>
 */
public interface TreeDao<T extends TreeEntity<T>> extends CrudDao<T> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 找到所有子节点
     *
     * @param entity 实体
     */
    List<T> findByParentIdsLike(T entity);

    /**
     * 更新所有父节点字段
     *
     * @param entity 实体
     */
    int updateParentIds(T entity);
}
