package com.zeyu.framework.core.security.cache;

import com.google.common.collect.Sets;
import com.zeyu.framework.core.web.servlet.Servlets;
import com.zeyu.framework.utils.SerializeUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 自定义授权缓存管理类，基于redis cache
 * Created by zeyuphoenix on 16/6/27.
 */
public class RedisCacheManager implements CacheManager {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /**
     * cache key start.
     */
    private String cacheKeyPrefix = "shiro_cache_";

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        return new RedisCache<>(cacheKeyPrefix + name);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getCacheKeyPrefix() {
        return cacheKeyPrefix;
    }

    public void setCacheKeyPrefix(String cacheKeyPrefix) {
        this.cacheKeyPrefix = cacheKeyPrefix;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    /**
     * 自定义授权缓存管理类
     */
    public class RedisCache<K, V> implements Cache<K, V> {

        // ================================================================
        // Constants
        // ================================================================

        /**
         * Logger
         */
        private Logger logger = LoggerFactory.getLogger(getClass());

        // ================================================================
        // Fields
        // ================================================================

        // cache 名称
        private String cacheKeyName = null;

        @Autowired
        private RedisTemplate<String, String> redisTemplate;

        // ================================================================
        // Constructors
        // ================================================================

        /**
         */
        public RedisCache(String cacheKeyName) {
            this.cacheKeyName = cacheKeyName;
            logger.debug("Init: cacheKeyName {} ", cacheKeyName);
        }

        // ================================================================
        // Methods from/for super Interfaces or SuperClass
        // ================================================================

        @SuppressWarnings("unchecked")
        @Override
        public V get(K key) throws CacheException {
            if (key == null) {
                return null;
            }

            V v;
            HttpServletRequest request = Servlets.getRequest();
            if (request != null) {
                v = (V) request.getAttribute(cacheKeyName);
                if (v != null) {
                    return v;
                }
            }

            V value = null;
            try {
                value = (V) redisTemplate.opsForHash().get(cacheKeyName, key);
                logger.debug("get {} {} {}", cacheKeyName, key, request != null ? request.getRequestURI() : "");
            } catch (Exception e) {
                logger.error("get {} {} {}", cacheKeyName, key, request != null ? request.getRequestURI() : "", e);
            }

            if (request != null && value != null) {
                request.setAttribute(cacheKeyName, value);
            }

            return value;
        }

        @Override
        public V put(K key, V value) throws CacheException {
            if (key == null) {
                return null;
            }

            try {

                redisTemplate.opsForHash().put(cacheKeyName, key, SerializeUtils.serializeToString((Serializable) value));
                logger.debug("put {} {} = {}", cacheKeyName, key, value);
            } catch (Exception e) {
                logger.error("put {} {}", cacheKeyName, key, e);
            }
            return value;
        }

        @SuppressWarnings("unchecked")
        @Override
        public V remove(K key) throws CacheException {
            V value = null;
            try {

                value = SerializeUtils.deserializeFromString(redisTemplate.opsForHash().get(cacheKeyName, key).toString());
                redisTemplate.opsForHash().delete(cacheKeyName, key);
                logger.debug("remove {} {}", cacheKeyName, key);
            } catch (Exception e) {
                logger.warn("remove {} {}", cacheKeyName, key, e);
            }
            return value;
        }

        @Override
        public void clear() throws CacheException {
            try {
                redisTemplate.opsForHash().delete(cacheKeyName);
                logger.debug("clear {}", cacheKeyName);
            } catch (Exception e) {
                logger.error("clear {}", cacheKeyName, e);
            }
        }

        @Override
        public int size() {
            int size = 0;
            try {
                size = redisTemplate.opsForHash().size(cacheKeyName).intValue();
                logger.debug("size {} {} ", cacheKeyName, size);
                return size;
            } catch (Exception e) {
                logger.error("clear {}", cacheKeyName, e);
            }
            return size;
        }

        @SuppressWarnings("unchecked")
        @Override
        public Set<K> keys() {
            Set<K> keys = Sets.newHashSet();
            try {

                Set<Object> set = redisTemplate.opsForHash().keys(cacheKeyName);
                keys.addAll(set.stream()
                        .map(key -> (K) key)
                        .collect(Collectors.toList())
                );
                logger.debug("keys {} {} ", cacheKeyName, keys);
                return keys;
            } catch (Exception e) {
                logger.error("keys {}", cacheKeyName, e);
            }
            return keys;
        }

        @SuppressWarnings("unchecked")
        @Override
        public Collection<V> values() {
            Collection<V> vals = Collections.emptyList();
            try {
                List<Object> values = redisTemplate.opsForHash().values(cacheKeyName);
                for (Object val : values) {
                    vals.add(SerializeUtils.deserializeFromString(val.toString()));
                }
                logger.debug("values {} {} ", cacheKeyName, vals);
                return vals;
            } catch (Exception e) {
                logger.error("values {}", cacheKeyName, e);
            }
            return vals;
        }
    }

    // ================================================================
    // Test Methods
    // ================================================================

}
