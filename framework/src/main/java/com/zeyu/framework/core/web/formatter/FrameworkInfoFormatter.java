package com.zeyu.framework.core.web.formatter;

import com.zeyu.framework.core.common.FrameworkInfo;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Formatters提供和PropertyEditor类似的功能，但是提供线程安全特性，并且专注于完成字符串和对象类型的互相转换
 * 通过FormatterRegistry将我们自己的formtter注册到系统中，然后Spring会自动完成文本表示的实体对象之间的互相转换
 * 如果需要通用类型的转换——例如String或Boolean，最好使用PropertyEditor完成，因为这种需求可能不是全局需要的，
 * 只是某个Controller的定制功能需求
 * Created by zeyuphoenix on 16/7/30.
 */
public class FrameworkInfoFormatter implements Formatter<FrameworkInfo> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /**
     * parse用于将字符串 name 转换成FrameworkInfo对象
     */
    @Override
    public FrameworkInfo parse(String text, Locale locale) throws ParseException {
        FrameworkInfo frameworkInfo = new FrameworkInfo();
        frameworkInfo.setName(text);
        return frameworkInfo;
    }

    /**
     * print用于将FrameworkInfo对象转换成该FrameworkInfo对应的 name 字符串
     */
    @Override
    public String print(FrameworkInfo frameworkInfo, Locale locale) {
        return frameworkInfo.getName();
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
