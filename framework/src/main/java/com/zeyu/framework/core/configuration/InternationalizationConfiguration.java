package com.zeyu.framework.core.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

/**
 * internationalization configuration
 * Created by zeyuphoenix on 16/6/19.
 */
@Configuration
public class InternationalizationConfiguration extends WebMvcConfigurerAdapter {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 国际化的消息资源
     */
    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageBundle = new ReloadableResourceBundleMessageSource();
        // 在web环境中一定要定位到classpath,否则在默认的当前web应用下寻找
        messageBundle.setBasename("classpath:message/messages");
        messageBundle.setDefaultEncoding("UTF-8");
        messageBundle.setCacheSeconds(60);
        messageBundle.setUseCodeAsDefaultMessage(true);
        return messageBundle;
    }

    /**
     * 基于session的拦截器
     */
    @Bean
    public LocaleResolver localeResolver() {

        // in controller used localeResolver.setLocale(request, response, Locale.ENGLISH)
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.SIMPLIFIED_CHINESE);
        return slr;
    }

    /**
     * 修改用户语言
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("changeLang");
        return lci;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
