package com.zeyu.framework.core.distributed;

import org.apache.shiro.session.mgt.SimpleSession;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * shiro每次访问都会多次调用SessionDAO的doUpdate方法，来更新Redis上数据，而更新的字段只有LastAccessTime（最后一次访问时间），
 * 由于会话失效是由Redis数据过期实现的，这个字段意义不大，为了减少对Redis的访问，降低网络压力，实现自己的Session，
 * 在SimpleSession上套一层，增加一个标识位，如果Session除lastAccessTime意外其它字段修改，就标识一下，
 * 只有标识为修改的才可以通过doUpdate访问Redis，否则直接返回。这也是上面SessionDao中doUpdate中逻辑判断的意义
 * <p>
 * 由于SimpleSession lastAccessTime更改后也会调用SessionDao update方法，
 * 增加标识位，如果只是更新lastAccessTime SessionDao update方法直接返回
 * <p>
 * Session Attribute
 * DefaultSubjectContext.PRINCIPALS_SESSION_KEY 保存 principal
 * DefaultSubjectContext.AUTHENTICATED_SESSION_KEY 保存 boolean是否登陆
 *
 * @see org.apache.shiro.subject.support.DefaultSubjectContext
 * Created by zeyuphoenix on 16/7/6.
 */
public class ShiroSession extends SimpleSession {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // 除lastAccessTime以外其他字段发生改变时为true
    private boolean isChanged;

    // ================================================================
    // Constructors
    // ================================================================

    public ShiroSession() {
        super();
        this.setChanged(true);
    }

    public ShiroSession(String host) {
        super(host);
        this.setChanged(true);
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================


    @Override
    public void setId(Serializable id) {
        super.setId(id);
        this.setChanged(true);
    }

    @Override
    public String getId() {
        return (String) super.getId();
    }

    @Override
    public void setStopTimestamp(Date stopTimestamp) {
        super.setStopTimestamp(stopTimestamp);
        this.setChanged(true);
    }

    @Override
    public void setExpired(boolean expired) {
        super.setExpired(expired);
        this.setChanged(true);
    }

    @Override
    public void setTimeout(long timeout) {
        super.setTimeout(timeout);
        this.setChanged(true);
    }

    @Override
    public void setHost(String host) {
        super.setHost(host);
        this.setChanged(true);
    }

    @Override
    public void setAttributes(Map<Object, Object> attributes) {
        super.setAttributes(attributes);
        this.setChanged(true);
    }

    @Override
    public void setLastAccessTime(Date lastAccessTime) {

        if (getLastAccessTime() != null) {
            long last = getLastAccessTime().getTime();
            long now = lastAccessTime.getTime();

            //如果3s内访问 则不更新session,否则需要更新远端过期时间
            if ((last - now) / 1000 >= 3) {
                //发送通知
                this.setChanged(true);
            }
        }
        super.setLastAccessTime(lastAccessTime);
    }

    @Override
    public Object removeAttribute(Object key) {
        this.setChanged(true);
        return super.removeAttribute(key);
    }

    /**
     * 防止过于频繁的保存
     */
    @Override
    public void setAttribute(Object key, Object value) {
        Object obj = this.getAttribute(key);
        if (obj != null && obj.equals(value)) {
            return;
        }
        super.setAttribute(key, value);
        this.setChanged(true);
    }

    /**
     * 停止
     */
    @Override
    public void stop() {
        super.stop();
        this.setChanged(true);
    }

    /**
     * 设置过期
     */
    @Override
    protected void expire() {
        this.stop();
        this.setExpired(true);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected boolean onEquals(SimpleSession ss) {
        return super.onEquals(ss);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public boolean isChanged() {
        return isChanged;
    }

    public void setChanged(boolean isChanged) {
        this.isChanged = isChanged;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
