package com.zeyu.framework.core.security.interfaces;

import com.zeyu.framework.core.security.session.SessionDAO;

import java.util.List;

/**
 * shiro权限控制的服务接口,必须实现
 * Created by zeyuphoenix on 16/7/29.
 */
public interface SecurityService {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * 加密常量
     */
    String HASH_ALGORITHM = "SHA-1";
    int HASH_INTERATIONS = 1024;
    int SALT_SIZE = 8;

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取用户信息
     */
    SecurityUser getUserByLoginName(String username);

    /**
     * 获取当前session信息
     */
    SessionDAO getSessionDao();

    /**
     * 更新用户登录信息
     */
    void updateUserLoginInfo(SecurityUser user);

    /**
     * 获取用户的菜单列表
     */
    List<String> findAllMenuPermissions();
}
