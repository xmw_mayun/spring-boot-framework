package com.zeyu.framework.core.common;

import java.io.Serializable;

/**
 * 框架的信息容器,可以保存框架、版本、作者等信息
 * Created by zeyuphoenix on 16/7/30.
 */
public class FrameworkInfo implements Serializable {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /**
     * 项目名称
     */
    private String name = "Framework";
    /**
     * 版本
     */
    private String version = "1.0";
    /**
     * 授权
     */
    private String license = "Apache License";
    /**
     * 项目描述
     */
    private String description = "Spring boot --> SpringMVC、MyBatis、Shiro、Redis、Mysql、MongoDB、Quartz and so on.";
    /**
     * 用户名
     */
    private String username = "zeyuphoenix";
    /**
     * 邮件
     */
    private String mail = "zeyuphoenix@163.com";
    /**
     * 网址
     */
    private String url = "www.zeyuphoenix.com";

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public String toString() {
        return "FrameworkInfo{" +
                "name='" + name + '\'' +
                ", version='" + version + '\'' +
                ", license='" + license + '\'' +
                ", description='" + description + '\'' +
                ", username='" + username + '\'' +
                ", mail='" + mail + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
