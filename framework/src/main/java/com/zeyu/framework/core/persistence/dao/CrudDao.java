package com.zeyu.framework.core.persistence.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * DAO支持类实现
 */
public interface CrudDao<T> extends BaseDao {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取单条数据
     *
     * @param id 对象Id
     * @return 对象
     */
    T get(String id);

    /**
     * 获取单条数据
     *
     * @param entity 对象实体
     * @return 数据
     */
    T get(T entity);

    /**
     * 查询数据列表，如果需要分页，请设置分页对象，如：entity.setPage(new Page<T>());
     *
     * @param entity 对象实体
     * @return 数据列表
     */
    List<T> findList(T entity);

    /**
     * 查询所有数据列表
     *
     * @param entity 对象实体
     * @return 数据列表
     */
    List<T> findAllList(T entity);

    /**
     * 查询所有数据列表
     *
     * @return 数据列表
     * @see public List<T> findAllList(T entity)
     */
    @Deprecated
    List<T> findAllList();

    /**
     * 插入数据
     *
     * @param entity 对象实体
     * @return 成功计数
     */
    int insert(T entity);

    /**
     * 更新数据
     *
     * @param entity 对象实体
     * @return 成功计数
     */
    int update(T entity);

    /**
     * 删除数据（一般为逻辑删除，更新del_flag字段为1）
     *
     * @param id id
     * @return 删除条数
     * @see public int delete(T entity)
     */
    // @Deprecated
    int delete(String id);

    /**
     * 删除数据（一般为逻辑删除，更新del_flag字段为1）
     *
     * @param entity 对象
     * @return 删除条数
     */
    int delete(T entity);

    /**
     * 删除数据（一般为逻辑删除，更新del_flag字段为1）
     *
     * @param idList 对象id列表
     * @return 删除条数
     */
    int deleteList(@Param("idList") List<String> idList, @Param("entity") T entity);
}
