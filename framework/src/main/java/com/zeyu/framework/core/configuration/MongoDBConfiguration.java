package com.zeyu.framework.core.configuration;

import org.springframework.context.annotation.Configuration;

/**
 * mongodb plugin configuration
 * Note:
 * <pre>
 *  Spring boot 自动加载 mongodb 的知识点:
 *      1. @ConditionOnClass 表明该@Configuration仅仅在一定条件下才会被加载，这里的条件是Mongo.class位于类路径上
 *      2. @EnableConfigurationProperties 将Spring Boot的配置文件（application.properties）中的
 *          spring.data.mongodb.*属性映射为MongoProperties并注入到MongoAutoConfiguration中。
 *      3. @ConditionalOnMissingBean 说明Spring Boot仅仅在当前上下文中不存在Mongo对象时，才会实例化一个Bean。
 *          这个逻辑也体现了Spring Boot的另外一个特性——自定义的Bean优先于框架的默认配置，我们如果显式的在业务代码中
 *          定义了一个Mongo对象，那么Spring Boot就不再创建
 *  MongoTemplate、Mongo、MongoFactory 都可以可以直接使用了
 * </pre>
 * 经过验证,这个版本的spring boot 可以直连2和3了,不用额外配置jar
 * 可以解开注释掉的代码,重新构建options进行自定义的配置.
 * Created by zeyuphoenix on 16/7/5.
 */
@Configuration
public class MongoDBConfiguration {

    // ================================================================
    // Constants
    // ================================================================

    /** admin数据库名称 */
    public static final String AUTHENTICATION_DATABASE_NAME = "admin";

    /** local数据库名称 */
    public static final String LOCAL_DATABASE_NAME = "local";

    // ================================================================
    // Fields
    // ================================================================

   /* @Autowired
    private MongoProperties properties;

    @Autowired(required = false)
    private MongoClientOptions options;

    @Autowired
    private Environment environment;

    private MongoClient mongo;*/

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

   /* @PreDestroy
    public void close() {
        if (this.mongo != null) {
            this.mongo.close();
        }
    }

    @Bean
    public MongoClient mongo() throws UnknownHostException {

        this.mongo = this.properties.createMongoClient(this.options, this.environment);
        return this.mongo;
    }*/

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
