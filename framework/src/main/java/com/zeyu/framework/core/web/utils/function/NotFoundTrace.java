package com.zeyu.framework.core.web.utils.function;

import org.beetl.core.Context;
import org.beetl.core.Function;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

/**
 * 404错误处理
 * Created by zeyuphoenix on 2016/3/30.
 */
public class NotFoundTrace implements Function {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public Object call(Object[] paras, Context ctx) {
        // 参数为空
        if (paras == null || paras.length == 0) {
            return "";
        }
        HttpServletResponse response = (HttpServletResponse) ctx.getGlobal("response");

        response.setStatus(404);

        if (paras[0] != null && Boolean.valueOf(paras[0].toString())) {
            try {
                response.getOutputStream().print("页面不存在.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return "";
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
