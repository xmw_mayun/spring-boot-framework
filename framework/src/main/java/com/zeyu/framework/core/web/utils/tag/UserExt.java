package com.zeyu.framework.core.web.utils.tag;

import com.zeyu.framework.core.security.SystemAuthorizingRealm;
import com.zeyu.framework.core.security.utils.SecurityUtils;
import com.zeyu.framework.core.web.servlet.Servlets;
import com.zeyu.framework.modules.sys.entity.User;
import com.zeyu.framework.modules.sys.utils.UserUtils;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * beetl 使用自定义函数写了user tag功能，你可以像使用jsp的标签那样使用
 * <!--:
 * if(user.getUser()) {}
 * -->
 * Created by zeyuphoenix on 2016/3/23.
 */
@Service
public class UserExt {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取当前用户对象
     *
     * @return 用户
     */
    public static User getUser() {
        return UserUtils.getUser();
    }

    /**
     * 获取当前用户对象是否是admin
     *
     * @return 是否
     */
    public static boolean isAdmin() {
        return UserUtils.getUser().isAdmin();
    }


    /**
     * 根据Id获取用户对象
     *
     * @return 用户
     */
    public static User getUserById(String id) {
        return UserUtils.get(id);
    }

    /**
     * 获取当前Principal
     *
     * @return 用户
     */
    public static SystemAuthorizingRealm.Principal getPrincipal() {
        return SecurityUtils.getPrincipal();
    }

    /**
     * 获取当前用户的菜单对象列表
     *
     * @return 菜单对象列表
     */
    public static List<?> getMenuList() {
        return UserUtils.getMenuList();
    }

    /**
     * 获取当前用户的部门对象列表
     *
     * @return 部门对象列表
     */
    public static List<?> getOfficeList() {
        return UserUtils.getOfficeList();
    }

    /**
     * 获取当前用户的角色对象列表
     *
     * @return 部门对象列表
     */
    public static List<?> getRoleList() {
        return UserUtils.getRoleList();
    }

    /**
     * 获取当前用户缓存
     *
     * @return 当前用户缓存
     */
    public static Object getCache(String cacheName, Object defaultValue) {
        return UserUtils.getCache(cacheName, defaultValue);
    }

    /**
     * 获取当前用户缓存
     *
     * @return 当前用户缓存
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {
        return Servlets.isAjaxRequest(request);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
