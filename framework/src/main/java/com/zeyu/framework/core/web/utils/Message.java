package com.zeyu.framework.core.web.utils;

import org.springframework.ui.Model;

import static com.zeyu.framework.core.web.BaseController.MESSAGE_KEY;

/**
 * 向model内部添加消息,扩展表单提示消息的问题
 * Created by zeyuphoenix on 2016/3/25.
 */
public class Message {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // spring model, save message
    private Model model;

    // ================================================================
    // Constructors
    // ================================================================

    /**
     * constructor
     *
     * @param model exist model
     */
    public Message(Model model) {
        this.model = model;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * add message in model
     *
     * @param key   message key
     * @param value message value
     * @return message
     */
    public Message addMessage(String key, String value) {

        // add your defined validation
        Object object = model.asMap().get(MESSAGE_KEY);
        if (object == null) {
            model.addAttribute(MESSAGE_KEY, key + ":" + value);
        } else {
            if (object.toString().endsWith("<br/>")) {
                model.addAttribute(MESSAGE_KEY, object + key + ":" + value);
            } else {
                model.addAttribute(MESSAGE_KEY, object + "<br/>" + key + ":" + value);
            }
        }

        return this;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
