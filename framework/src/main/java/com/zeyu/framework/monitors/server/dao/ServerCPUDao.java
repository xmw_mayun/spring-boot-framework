package com.zeyu.framework.monitors.server.dao;

import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.monitors.server.entity.CPUStatus;

import java.util.List;

/**
 * server cpu 状态的处理dao
 * Created by zeyuphoenix on 16/8/27.
 */
@MyBatisDao
public interface ServerCPUDao extends CrudDao<CPUStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取类型列表
     *
     * @return 类型列表
     */
    List<String> findIndList();

}
