package com.zeyu.framework.monitors.redis.dao;

import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.monitors.redis.entity.RedisStatus;
import com.zeyu.framework.utils.mybatis.annotation.DaoAutoGenerate;

/**
 * redis 状态的处理dao
 * Created by zeyuphoenix on 16/8/27.
 */
@MyBatisDao
public interface RedisDao extends CrudDao<RedisStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

}
