package com.zeyu.framework.monitors.server.utils;

/**
 * helper class to check the operating system this Java VM runs in
 * http://stackoverflow.com/questions/228477/how-do-i-programmatically-determine-operating-system-in-java
 * compare to http://svn.terracotta.org/svn/tc/dso/tags/2.6.4/code/base/common/src/com/tc/util/runtime/Os.java
 * http://www.docjar.com/html/api/org/apache/commons/lang/SystemUtils.java.html
 */
public final class OsCheck {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    protected static OSType detectedOS;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * detected the operating system from the os.name System property and cache
     * the result
     * @return - the operating system detected
     */
    public static OSType getOperatingSystemType() {
        if (detectedOS == null) {
            String OS = System.getProperty("os.name", "generic").toLowerCase();
            if (OS.contains("win")) {
                detectedOS = OSType.Windows;
            } else if ((OS.contains("mac")) || (OS.contains("darwin"))) {
                detectedOS = OSType.MacOS;
            } else if (OS.contains("nux")) {
                detectedOS = OSType.Linux;
            } else {
                detectedOS = OSType.Other;
            }
        }
        return detectedOS;
    }

    /**
     * 根据系统返回目录
     */
    public static String getOperatingSystemFile() {

        OSType osType = getOperatingSystemType();
        if (osType == OSType.Windows) {
            return "win";
        } else if (osType == OSType.MacOS) {
            return "mac";
        }

        return "linux";
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    /**
     * types of Operating Systems
     */
    public enum OSType {

        // ================================================================
        // Constants
        // ================================================================

        Windows, MacOS, Linux, Other
    }

    // ================================================================
    // Test Methods
    // ================================================================

}
