package com.zeyu.framework.monitors.webserver.entity;

import com.zeyu.framework.core.persistence.entity.SimpleEntity;

/**
 * 请求进程状态
 * Created by zeyuphoenix on 16/9/29.
 */
public class RequestProcessorStatus extends SimpleEntity {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 状态号
    private String stage;
    // 请求处理时间
    private String requestProcessingTime;
    // 请求发送字节
    private String requestBytesSent;
    // 请求接收字节
    private String requestBytesReceived;
    // 远程转发
    private String remoteAddrForwarded;
    // 远程地址
    private String remoteAddr;
    // 虚拟主机
    private String virtualHost;
    // 方式
    private String method;
    // 请求url
    private String currentUri;
    // 请求参数
    private String currentQueryString;
    // 请求协议
    private String protocol;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getRequestProcessingTime() {
        return requestProcessingTime;
    }

    public void setRequestProcessingTime(String requestProcessingTime) {
        this.requestProcessingTime = requestProcessingTime;
    }

    public String getRequestBytesSent() {
        return requestBytesSent;
    }

    public void setRequestBytesSent(String requestBytesSent) {
        this.requestBytesSent = requestBytesSent;
    }

    public String getRequestBytesReceived() {
        return requestBytesReceived;
    }

    public void setRequestBytesReceived(String requestBytesReceived) {
        this.requestBytesReceived = requestBytesReceived;
    }

    public String getRemoteAddrForwarded() {
        return remoteAddrForwarded;
    }

    public void setRemoteAddrForwarded(String remoteAddrForwarded) {
        this.remoteAddrForwarded = remoteAddrForwarded;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public String getVirtualHost() {
        return virtualHost;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCurrentUri() {
        return currentUri;
    }

    public void setCurrentUri(String currentUri) {
        this.currentUri = currentUri;
    }

    public String getCurrentQueryString() {
        return currentQueryString;
    }

    public void setCurrentQueryString(String currentQueryString) {
        this.currentQueryString = currentQueryString;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
