package com.zeyu.framework.monitors.server.service;

import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.core.service.CrudService;
import com.zeyu.framework.monitors.server.dao.ServerDao;
import com.zeyu.framework.monitors.server.entity.ServerStatus;
import com.zeyu.framework.utils.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 处理server监控的service
 * Created by zeyuphoenix on 16/8/27.
 */
@Service
@Transactional(readOnly = true)
public class ServerService extends CrudService<ServerDao, ServerStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Override
    public Page<ServerStatus> findPage(Page<ServerStatus> page, ServerStatus serverStatus) {

        // 设置默认时间范围，默认当前月
        if (serverStatus.getBeginDate() == null) {
            serverStatus.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
        }
        if (serverStatus.getEndDate() == null) {
            serverStatus.setEndDate(DateUtils.addMonths(serverStatus.getBeginDate(), 1));
        }

        return super.findPage(page, serverStatus);

    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
