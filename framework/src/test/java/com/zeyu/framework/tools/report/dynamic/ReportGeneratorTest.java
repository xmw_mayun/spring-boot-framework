package com.zeyu.framework.tools.report.dynamic;

import com.google.common.collect.Lists;
import com.zeyu.framework.core.persistence.table.Field;
import com.zeyu.framework.tools.report.charts.ChartData;
import com.zeyu.framework.tools.report.charts.ChartUtils;
import com.zeyu.framework.tools.report.charts.ExtendChartData;
import com.zeyu.framework.tools.report.convert.Converter;
import com.zeyu.framework.utils.dynacompile.DynamicEngineTest;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.ImageIcon;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * 生成报表测试
 * Created by zeyuphoenix on 16/9/4.
 */
public class ReportGeneratorTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ReportGeneratorTest.class);

    @Test
    public void generate() {
        Locale.setDefault(Locale.CHINA);

        logger.info("generate report start");

        List<ComponentBuilder<?, ?>> builders = Lists.newArrayList();

        URL url = DynamicEngineTest.class.getClassLoader().getResource("");

        if (url != null) {
            String path = url.getPath();
            // //////////////////////////////////////////
            // create
            // //////////////////////////////////////////

            ImageIcon icon1 = new ImageIcon(path + File.separator + "539.png");
            ComponentBuilder<?, ?> builder = ReportGenerator.createImageComponent("Bar 1 Demo",
                    icon1);
            builders.add(builder);
            builder = ReportGenerator.createTitleComponent("test发大水发大水");
            builders.add(builder);
            builder = ReportGenerator.createTableComponent("table", createDataSource());
            builders.add(builder);
            builder = ReportGenerator.createTableComponent("table2", createDataSource2());
            builders.add(builder);

            ComponentBuilder<?, ?>[] buildArray = new ComponentBuilder<?, ?>[builders
                    .size()];
            builders.toArray(buildArray);
            ReportGenerator.buildPDF("Test 大幅度", path + File.separator + "112.pdf",
                    buildArray);
            ReportGenerator.buildXLS("Test 大幅度", path + File.separator + "112.xls",
                    buildArray);
        }
        logger.info("generate report end");
    }

    @Test
    public void generateChartandTable() {
        ExtendChartData extendChartData = createChartData();

        List<ComponentBuilder<?, ?>> builders = Lists.newArrayList();

        try {
            URL url = DynamicEngineTest.class.getClassLoader().getResource("");

            if (url != null) {
                String path = url.getPath();
                // convert
                String output = Converter.getInstance().convert(extendChartData);

                byte[] bytes = ChartUtils.generateImage(output);
                logger.debug("out bytes: ", bytes);

                ImageIcon icon = new ImageIcon(bytes);
                logger.debug("icon is: ", icon);

                // //////////////////////////////////////////
                // create
                // //////////////////////////////////////////

                ComponentBuilder<?, ?> builder = ReportGenerator.createImageComponent("用户流量统计图",
                        icon);
                builders.add(builder);
                builder = ReportGenerator.createTitleComponent("信息信息");
                builders.add(builder);
                builder = ReportGenerator.createTableComponent("用户流量统计表", ReportGenerator.createDataSource(extendChartData));
                builders.add(builder);

                ComponentBuilder<?, ?>[] buildArray = new ComponentBuilder<?, ?>[builders.size()];
                builders.toArray(buildArray);
                ReportGenerator.buildPDF("用户流量信息图", path + File.separator + "用户流量.pdf", buildArray);
                ReportGenerator.buildXLS("用户流量信息图", path + File.separator + "用户流量.xls", buildArray);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static ReportGenerator.FieldDataSource createDataSource() {

        Field[] selectKeys = new Field[5];
        selectKeys[0] = new Field("用户", Field.FieldType.ANGLE);
        selectKeys[1] = new Field("上行流量", Field.FieldType.QUOTA);
        Field.Render render = new Field.Render("flowTransform");
        selectKeys[1].setRender(render);
        selectKeys[2] = new Field("下行流量", Field.FieldType.QUOTA);
        selectKeys[2].setRender(render);
        render = new Field.Render("packetTransform");
        selectKeys[3] = new Field("上行报文", Field.FieldType.QUOTA);
        selectKeys[3].setRender(render);
        selectKeys[4] = new Field("下行报文", Field.FieldType.QUOTA);
        selectKeys[4].setRender(render);

        Object[][] datas = new Object[8][];
        datas[0] = new Object[]{"张三", 1200, 2566, 452, 15665};
        datas[1] = new Object[]{"Baby", 200, 2566, 4152, 145};
        datas[2] = new Object[]{"宝贝", 212010, 21566, 4252, 1445};
        datas[3] = new Object[]{"Kaer", 132200, 23566, 52, 1335};
        datas[4] = new Object[]{"李四", 1434351200, 62566, 42, 1215};
        datas[5] = new Object[]{"Smith", 61200, 72566, 4152, 1335};
        datas[6] = new Object[]{"Jone", 17200, 82566, 2452, 151};
        datas[7] = new Object[]{"王五", 2200, 566, 4852, 125};

        return ReportGenerator.createDataSource(selectKeys, datas);
    }

    private static ReportGenerator.FieldDataSource createDataSource2() {

        Field[] selectKeys = new Field[4];
        selectKeys[0] = new Field("item", Field.FieldType.ANGLE);
        selectKeys[1] = new Field("quantity", Field.FieldType.QUOTA);
        selectKeys[2] = new Field("unitprice", Field.FieldType.QUOTA);
        selectKeys[3] = new Field("sample", Field.FieldType.OTHER);

        ReportGenerator.FieldDataSource dataSource = new ReportGenerator.FieldDataSource(selectKeys);
        dataSource.add("Book like", 170, new BigDecimal(100),
                "Book sample like");
        dataSource.add("Notebook", 90, new BigDecimal(450), "Notebook");
        dataSource.add("PDA", 120, new BigDecimal(250), "PDA");
        dataSource.add("Book1", 170, new BigDecimal(100), "Book");
        dataSource.add("Notebook1", 90, new BigDecimal(450), "Notebook");
        dataSource.add("PDA1", 120, new BigDecimal(250), "good or bad.");
        return dataSource;
    }

    /**
     * 创建图数据
     */
    private static ExtendChartData createChartData() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"一月", "二月", "三月", "四月",
                "五月", "六月", "七月", "八月", "九月"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();
        CollectionUtils.addAll(serie1, new Number[]{1200, 2320, 301000000, 434,
                39000, 53000000, 32000000, 120, 250});
        List<Number> serie2 = Lists.newArrayList();
        CollectionUtils.addAll(serie2, new Number[]{15000, 100060, 100090, 150,
                2000032, 20000001, 1000054, 390, 10000010});

        series.add(serie1);
        series.add(serie2);

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("line");
        extendChartData.setTilte("用户流量趋势图");
        List<String> legends = Lists.newArrayList();
        legends.add("上行流量");
        legends.add("下行流量");
        // legends
        extendChartData.setLegends(legends);
        extendChartData.setDatas(datas);

        return extendChartData;
    }
}