package com.zeyu.framework.modules.sys.service;

import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.modules.sys.entity.Log;
import com.zeyu.framework.modules.sys.entity.User;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;

/**
 * log service test
 * Created by zeyuphoenix on 16/8/3.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@FixMethodOrder(MethodSorters.DEFAULT)   //按方法顺序执行
public class LogServiceTest {


    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(LogServiceTest.class);

    @Autowired
    private LogService logService;

    @Test
    public void findPage() throws Exception {
        Page<Log> page = new Page<>();
        Page<Log> dictList = logService.findPage(page, new Log());
        if (dictList != null) {
            logger.info("query page list is {}", dictList);
        }
    }

    @Test
    public void save() throws Exception {

        Log log = new Log();
        log.setId("test-12345");
        log.setType("1");
        log.setIsNewRecord(true);
        log.setCreateBy(new User("1"));
        log.setUpdateBy(new User("1"));
        log.setCreateDate(new Date());
        log.setUpdateDate(new Date());

        logService.save(log);
    }

}