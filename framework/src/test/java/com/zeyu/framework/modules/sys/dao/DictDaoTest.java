package com.zeyu.framework.modules.sys.dao;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.modules.sys.entity.Dict;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * 无事务测试分页
 * Created by zeyuphoenix on 16/8/4.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.DEFAULT)   //按方法顺序执行
public class DictDaoTest {


    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(DictDaoTest.class);

    //
    @Autowired
    private  DictDao dictDao;

    @Test
    public void findPage() throws Exception {

        List<Dict> dictList = dictDao.findList(new Dict());
        if (dictList != null) {
            logger.info("query page list is {}", dictList);
        }
        Page<Dict> page = new Page<>();

        //获取第2页，5条内容，默认查询总数count
        PageHelper.startPage(25, page.getMaxResults());
        Dict dict = new Dict();
        // 显示为List,实际是Page对象
        dict.setPage(page);
        List<Dict> list = dictDao.findList(dict);

        //用PageInfo对结果进行包装
        PageInfo<Dict> pageInfo = new PageInfo<>(list);

        logger.info("query page is {}", pageInfo);
        logger.info("query page list is {}", pageInfo.getList());

    }

}