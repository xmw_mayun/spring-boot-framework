package com.zeyu.framework.utils;

import org.junit.Test;

/**
 * ip test
 * Created by zeyuphoenix on 16/7/21.
 */
public class IPUtilsTest {


    @Test
    public void test() {

        String ip = "192.168.111.155";
        int mask = IPUtils.toMask("255.255.0.0");
        byte[] bytes = IPUtils.toBytes(ip);
        int ip1 = IPUtils.toInt(bytes);
        int ip2 = IPUtils.toInt(ip);
        int networkNo = IPUtils.getNetworkNoToInt(ip, mask);
        String toIp = IPUtils.toIp(bytes);
        System.out.println(ip1 + "_" + ip2 + "\r\nip地址:" + toIp + "/" +
                mask + "\r\n网络号:" + IPUtils.toIp(networkNo) +
                "\r\n广播地址:" + IPUtils.toIp(IPUtils.broadcast(toIp, mask)) +
                "\r\n最小地址:" + IPUtils.toIp(IPUtils.minIpToInt(ip, mask)) +
                "\r\n最大地址:" + IPUtils.toIp(IPUtils.maxIpToInt(ip, mask)));

    }
}