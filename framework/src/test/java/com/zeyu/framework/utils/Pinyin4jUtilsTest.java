package com.zeyu.framework.utils;

import org.junit.Test;

/**
 * 测试转换
 * Created by zeyuphoenix on 2017/3/1.
 */
public class Pinyin4jUtilsTest {

    @Test
    public void converterToFirstSpell() throws Exception {
        String name = "北京市长";
        System.out.println(Pinyin4jUtils.converterToFirstSpell(name));
        name = "北京市长 i love";
        System.out.println(Pinyin4jUtils.converterToFirstSpell(name));
    }

    @Test
    public void converterToAllFirstSpell() throws Exception {
        String name = "北京市长";
        System.out.println(Pinyin4jUtils.converterToAllFirstSpell(name));
        name = "北京市长 i love";
        System.out.println(Pinyin4jUtils.converterToAllFirstSpell(name));
    }

    @Test
    public void converterToSpell() throws Exception {
        String name = "北京市长";
        System.out.println(Pinyin4jUtils.converterToSpell(name));
        name = "北京市长 i love";
        System.out.println(Pinyin4jUtils.converterToSpell(name));
    }

    @Test
    public void converterToAllSpell() throws Exception {
        String name = "北京市长";
        System.out.println(Pinyin4jUtils.converterToAllSpell(name));
        name = "北京市长 i love";
        System.out.println(Pinyin4jUtils.converterToAllSpell(name));
    }

    @Test
    public void getCnASCII() throws Exception {
        String name = "北京市长";
        System.out.println(Pinyin4jUtils.getCnASCII(name));
        name = "北京市长 i love";
        System.out.println(Pinyin4jUtils.getCnASCII(name));
    }

}