package com.zeyu.framework.utils.excel;

import com.google.common.collect.Lists;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * 导出 excel 测试
 * Created by zeyuphoenix on 2017/5/23.
 */
public class ExportExcelTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ExportExcelTest.class);

    @Test
    public void export() throws IOException {
        List<String> headerList = Lists.newArrayList();
        for (int i = 1; i <= 10; i++) {
            headerList.add("表头" + i);
        }

        List<String> dataRowList = Lists.newArrayList();
        for (int i = 1; i <= headerList.size(); i++) {
            dataRowList.add("数据" + i);
        }

        List<List<String>> dataList = Lists.newArrayList();
        for (int i = 1; i <= 100; i++) {
            dataList.add(dataRowList);
        }

        ExportExcel ee = new ExportExcel("表格标题", headerList);

        for (List<String> aDataList : dataList) {
            Row row = ee.addRow();
            for (int j = 0; j < aDataList.size(); j++) {
                ee.addCell(row, j, aDataList.get(j));
            }
        }

        ee.writeFile("target/export.xlsx");

        ee.dispose();

        logger.debug("Export success.");
    }
}