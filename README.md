# Spring boot framework study
### 1.由来

最近几年一直用 spring，发现 spring boot 出来了很好奇这玩意，就把用到的和学习到的用 spring boot 重写了一下，里面有很多注释就是学习的过程中写的，有的代码虽然多余，但为了学习也就不删除了，其实有的地方的注解 bean，spring boot 已经实现了，为了验证或者重置参数重写了一部分，希望可以帮助提高

### 2.包含

1. 前端    
    写css和布局一直是我心中的痛，没办法啊审美太差，这里用了前端的一个模板 smartadmin，包含很多东西の：   
JQuery、Bootstrap、echarts、datatables、ztree、treetable、validate、layer 等等，太多了自己看吧：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173003_302698f8_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173031_4c53107f_606783.png "在这里输入图片标题")

2.后端

    主要就是 spring-boot 的一大堆，比如 web、webmvc、validation、cache、aop、jdbc、data、mail、websocket、mq等等
    * JSON 相关 gson、jackson
    * 日志相关 logback、slf4j
    * 数据库连接池 druid
    * 模板 beetl
    * mybatis、mybatis-plugin 操作数据库
    * redis 的 driver
    * mongodb 的 driver
    * rabbitmq 的 driver
    * 权限用的 shiro 相关类
    * 报表用的 jasperreports + poi + itext, 还用了 phantomjs 动态生成报表图
    * 邮件相关的mail
    * 任务调度相关的 quartz
    * 一大堆公共库 apache commons 和 guava 等
    * 还有 api 相关的 swaggerui
    * 好像还有openid第三方登录、websocket、二维码、xml、监控 sigar、doc转换、console等，pom.xml 都有注释

### 3.截图
##### 登录图
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173135_99eea9aa_606783.png "在这里输入图片标题")

##### dashboard
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173155_f240edd9_606783.png "在这里输入图片标题")

##### mysql 监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173213_4c33b49b_606783.png "在这里输入图片标题")

##### redis监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173234_8825e5b2_606783.png "在这里输入图片标题")

##### mongodb 监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173251_f6e467d8_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173304_722f0201_606783.png "在这里输入图片标题")

##### 服务器监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173322_42e92c84_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173335_3030a8bc_606783.png "在这里输入图片标题")

##### tomcat监控
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173348_d22b4709_606783.png "在这里输入图片标题")

##### 定时任务列表
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173402_b181a97a_606783.png "在这里输入图片标题")

##### 定时任务历史
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173420_38eb81e2_606783.png "在这里输入图片标题")

##### 定时任务编辑
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173436_48ad5a74_606783.png "在这里输入图片标题")

##### 定时任务 quarz 表达式配置
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173451_fb99de53_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173509_cf398acf_606783.png "在这里输入图片标题")

##### 调试和日志
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173526_94dac785_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173540_495941bb_606783.png "在这里输入图片标题")

##### 动态调试注入
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173556_29b6509d_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173610_913507c1_606783.png "在这里输入图片标题")

##### 控制台列表
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173632_f4ad16c6_606783.png "在这里输入图片标题")

##### 远程主机配置
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173645_2dbe6abd_606783.png "在这里输入图片标题")

##### 远程主机ssh和屏幕键盘、sftp 上传下载
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173703_3acb842e_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173723_ddcac6c6_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173747_4994b67a_606783.png "在这里输入图片标题")

##### 远程主机 RDP 连接（VNC 类似）
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/092321_c8c3afaa_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/092332_e42f96e1_606783.png "在这里输入图片标题")

##### 阅读后台 pdf
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173843_3c51ff58_606783.png "在这里输入图片标题")

##### 通过 phantomjs 自动生成和前台效果一致的报表
![输入图片说明](https://git.oschina.net/uploads/images/2017/0607/173909_af95ac34_606783.png "在这里输入图片标题")

##### 简单的字典管理
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091156_9f0f7c89_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091215_0b4c865f_606783.png "在这里输入图片标题")

##### 日志查询
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091246_caf29218_606783.png "在这里输入图片标题")

##### 菜单管理
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091309_62fac24d_606783.png "在这里输入图片标题")

##### 角色管理
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091354_e5f14079_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091417_f612cd26_606783.png "在这里输入图片标题")

##### 用户管理
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091605_f511b51d_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091623_59cd81d4_606783.png "在这里输入图片标题")

##### 授权
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091522_abe57183_606783.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091540_bc2a1af8_606783.png "在这里输入图片标题")

##### 分配用户菜单
![输入图片说明](https://git.oschina.net/uploads/images/2017/0608/091450_338effa7_606783.png "在这里输入图片标题")

### 4.安装启动
   这个框架包含了一些乱七八糟的东西，全部启动还是比较麻烦的，当然可以把 application.properties 里的开关都设置为 false,启动就可以简单一点

1.首先你要会用 maven

    简单用的话，把 application.properties 里的mysql、redis、mongodb 改成你自己的配置，执行下` mvn package -DskipTests`
    打 jar 或者 war 需要改动 `pom.xml`，默认应该打 war 吧，我在阿里云打包的 war，因为 像 sigar 包含 dll，还有 phantomjs 是可执行程序，打 jar 的话需要用程序处理它们的位置

2.你要会用 mysql

    这个项目数据库用的 mysql, 因为用了 mybatis, 你懂得, 改成别的数据库不改动 mybatis的xml 是不可能的，怎么也会有特定的 sql 语法的

3.你要会用 tomcat or jetty

    一些常用的配置和脚本都放在 tools 下面了, 但是最起码你要会读啊, tomcat 的配置就是端口、SSL 和一些线程数的优化

4.你要会用 redis

    这个项目可以部署为分布式, 分布式缓存就用的 redis，起码要会安装的，配置脚本已经写了

5.你要会用 rabbitmq

    公司项目用到了 mq, 以前是 activeMQ, 太大了，现在改成 rabbitMQ 了，这项目了写了基本的监听,可以发送点对点、发布订阅、点对面的几种消息

6.你要会用 mongodb

    公司项目用到了 mongodb，这里也合入了,就是简单的合入，使用了 spring-data 的框架

7.你要会用 guacamole

    如果你需要远程控制台，想在网页上看 VNC\RDP\SSH, 请把 guacamole 的 server 编译，客户端已经写完了

8.你要会用 linux 的一些基本操作

    开发一般在 mac 上，部署一般在 centos 上，这些软件的安装命令要会的

9.你要会用 nginx

    因为实现了分布式部署，如果配置分布式，你要会用 nginx, 配置脚本已经写好了，改改 IP 就行了

    实现了一个nginx + tomcat + redis的分布式结构
     * 1. 最简单的分布式部署是基于web容器实现分布式session共享,但是这样实现有一些问题,
     * 具体看nginx的部署说明
     * 2. 此框架使用shiro的session替换掉web容器的session,通过shiro的session共享
     * 实现分布式部署
     * 3. 由于shiro框架获取session的属性时，每次都去拿取session，一次请求中会多次
     * 获取session里面的属性，这个如果是本地缓存到无所谓，因为本地缓存是直接放置
     * session对象的，但是如果是共享缓存比如redis，每次获取session都要从redis
     * 里面获取然后反序列化,会出现效率问题
     * 4. 这里使用redis作为shiro实现集群会话管理存储，并可配置ehcache作为进程内缓存，
     * 通过redis消息订阅发布实现session缓存统一


### 5.无聊的话

这个框架乱七八糟的东西很多，都是工作和学习中用到的，如果对你有帮助你是很好的，没有你能点进来看看也是缘分啊

